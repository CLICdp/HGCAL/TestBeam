# CMS HGCal Testbeam Analysis Framework

=============================================
* This new branch is to be used for analysing the injection data the rpi test stand. 

### Content

- [Download and install the code](#download-the-code)
- [Example to run on pedestal data](#pedestal-run)
- [Example to run on injection data](#injection-run)
 
## Download and install the code
* CMSSW Version 8.0.1

```bash
scram project ${RELEASE}
cd ${RELEASE}/src/
cmsenv
git cms-init
git clone https://gitlab.cern.ch/cms-hgcal-tb/TestBeam.git HGCal
cd HGCal
git checkout HexaboardTestLabAnalysis
git pull
scram b -j16
```

## Example to run on pedestal data:
```bash
cmsRun runPedestalLabDataAnalysis_cfg.py fileName=/eos/cms/store/group/dpg_hgcal/tb_hgcal/calibration/module78/raw/Module78_2-8-2018_12-11.raw  edmOutputFolder=${PWD} analysisOutputFolder=${PWD} electronicMap=HGCal/CondObjects/data/map_CERN_Hexaboard_OneModule_V3.txt

```

## Example to run on pedestal data for Noise-Analysis:
```bash
cmsRun runPedestalLabDataAnalysis_cfg.py fileName=/eos/cms/store/group/dpg_hgcal/tb_hgcal/calibration/module78/raw/Module78_2-8-2018_12-11.raw  edmOutputFolder=${PWD} analysisOutputFolder=${PWD} electronicMap=HGCal/CondObjects/data/EElayer2_emap.txt
cmsRun runNoiseLabDataAnalysis_cfg.py fileName=/eos/cms/store/group/dpg_hgcal/tb_hgcal/calibration/module78/raw/Module78_2-8-2018_12-11.raw  edmOutputFolder=${PWD} analysisOutputFolder=${PWD} electronicMap=HGCal/CondObjects/data/EElayer2_emap.txt

```

## Example to run on injection data:
```bash
cmsRun runInjectionLabDataAnalysis_cfg.py fileName=/eos/cms/store/group/dpg_hgcal/tb_hgcal/calibration/module78/raw/Module78_2-8-2018_13-36.raw  edmOutputFolder=${PWD} analysisOutputFolder=${PWD} electronicMap=HGCal/CondObjects/data/map_CERN_Hexaboard_OneModule_V3.txt

```

## Information on Noise-Analysis (HexaboardTestLabAnalysis-branch only):
electronicMap=HGCal/CondObjects/data/EElayer2_emap.txt is important! - This is the emap when looking at the hexaboard/sensor from top (as usual)
The pedestal - Analysis needs to be run before (at least once per sensor) to create the pedestal-file which is needed for the second step.


Chooseable options (only by editing the python script runNoiseLabDataAnalysis_cfg.py):
- NTSForPedestalComputation=cms.untracked.int32(7) , which represents how many time samples one wants to use
- fillFullTree=cms.untracked.bool(True) , which gives the option to print the tree with all calculated ADC-counts (unsorted)

The output tree will have the calculated values available with "all Mean"- case AND the "all Median" - case in different branches of the tree.

The standard version uses the "NoisePlotter" (to be found in RawToDigi/plugins/NoisePlotter.cc) which calculates common mode noise PER BOARD. 
In case a calculation PER ROC is desired the following steps need to be taken:
- in file runNoiseLabDataAnalysis_cfg.py , line 101 change ' process.noiseplotter = cms.EDAnalyzer("NoisePlotter",  ' to ' process.noiseplotter = cms.EDAnalyzer("NoisePerROCPlotter",  '

This should be sufficient for standard tests. It is possible to plot data without subtracting common Mode and/or pedestal. There, one need to edit the NoisePlotter.cc or the NoisePerROCPlotter.cc respectively. 
Around line 315 and 338 (for NoisePlotter), there are commented lines which prepare for this. (Just the 'else' - case is relevant) Afterwards, recompiling (!) is needed. To do so, run
```bash
scram b
```
in the HGCAL folder.
Depending on what is desired, some other lines might need to be commented (scram b tells you which ones) because declared, but unused variable that are treaten as in error in CMSSW.
