import uproot
import numpy as np
#import pandas as pd
import matplotlib
# matplotlib.use('agg')
import matplotlib.pyplot as plt
#import csv
from pathlib import Path

xArray=[10,50,100,150,200,250,300,350,400]
xArrayModule=[10,50,100,150,200,250,300,350]

# Sensor1111
p1111 = Path('/afs/cern.ch/user/p/psiebere/CMSSW_8_0_1/src/HGCal/voltage_scan/Sensor1111/root_files/') # enter directory here
files1111=[]
for i in p1111.iterdir():
    if i.match('*pedestal.root'):
        files1111.append(i)
dic1111={}
tree_dic1111 = {}
df_dic1111={}
# medianYArraySensor1111=[]
meanYArraySensor1111=[]
meanErrorYArraySensor1111=[]
for x in xArray:
    for i in files1111:
        if i.match('*' + str(x) + '_pedestal*'):
            myfile = i
    dic1111["f{0}".format(x)]=uproot.open(myfile)
    tree_dic1111["treeModule{0}".format(x)]=dic1111["f" + str(x)]["noiseplotter"]["noiseTree"]
    df_dic1111["df_Sensor1111_{0}".format(x)]=tree_dic1111["treeModule" + str(x)].pandas.df()
    # medianYArraySensor1111.append(df_dic1111["df_Sensor1111_{0}".format(x)]["IQR_high"].median())
    meanYArraySensor1111.append(df_dic1111["df_Sensor1111_{0}".format(x)]["RMS_high"].mean())
    meanErrorYArraySensor1111.append(df_dic1111["df_Sensor1111_{0}".format(x)]["RMS_high"].std())
# tree_dic1111["treeModule10"].show()
# print(medianYArraySensor1111)
print(meanYArraySensor1111)

# Sensor3001
p3001 = Path('/afs/cern.ch/user/p/psiebere/CMSSW_8_0_1/src/HGCal/voltage_scan/Sensor3001/root_files/') # enter directory here
files3001=[]
for i in p3001.iterdir():
    if i.match('*pedestal.root'):
        files3001.append(i)
dic3001={}
tree_dic3001 = {}
df_dic3001={}
# medianYArraySensor3001=[]
meanYArraySensor3001=[]
meanErrorYArraySensor3001=[]
for x in xArray:
    for i in files3001:
        if i.match('*' + str(x) + '_pedestal*'):
            myfile = i
    dic3001["f{0}".format(x)]=uproot.open(myfile)
    tree_dic3001["treeModule{0}".format(x)]=dic3001["f" + str(x)]["noiseplotter"]["noiseTree"]
    df_dic3001["df_Sensor3001_{0}".format(x)]=tree_dic3001["treeModule" + str(x)].pandas.df()
    # medianYArraySensor3001.append(df_dic3001["df_Sensor3001_{0}".format(x)]["IQR_high"].median())
    meanYArraySensor3001.append(df_dic3001["df_Sensor3001_{0}".format(x)]["RMS_high"].mean())
    meanErrorYArraySensor3001.append(df_dic3001["df_Sensor3001_{0}".format(x)]["RMS_high"].std())
# tree_dic3001["treeModule10"].show()
# print(medianYArraySensor3001)
print(meanYArraySensor3001)

# Sensor3008
p3008 = Path('/afs/cern.ch/user/p/psiebere/CMSSW_8_0_1/src/HGCal/voltage_scan/Sensor3008/root_files/') # enter directory here
files3008=[]
for i in p3008.iterdir():
    if i.match('*pedestal.root'):
        files3008.append(i)
dic3008={}
tree_dic3008 = {}
df_dic3008={}
# medianYArraySensor3008=[]
meanYArraySensor3008=[]
meanErrorYArraySensor3008=[]
for x in xArray:
    for i in files3008:
        if i.match('*' + str(x) + '_pedestal*'):
            myfile = i
    dic3008["f{0}".format(x)]=uproot.open(myfile)
    tree_dic3008["treeModule{0}".format(x)]=dic3008["f" + str(x)]["noiseplotter"]["noiseTree"]
    df_dic3008["df_Sensor3008_{0}".format(x)]=tree_dic3008["treeModule" + str(x)].pandas.df()
    # medianYArraySensor3008.append(df_dic3008["df_Sensor3008_{0}".format(x)]["IQR_high"].median())
    meanYArraySensor3008.append(df_dic3008["df_Sensor3008_{0}".format(x)]["RMS_high"].mean())
    meanErrorYArraySensor3008.append(df_dic3008["df_Sensor3008_{0}".format(x)]["RMS_high"].std())
# tree_dic3008["treeModule10"].show()
# print(medianYArraySensor3008)
print(meanYArraySensor3008)


# Sensor4008
p4008 = Path('/afs/cern.ch/user/p/psiebere/CMSSW_8_0_1/src/HGCal/voltage_scan/Sensor4008/root_files/') # enter directory here
files4008=[]
for i in p4008.iterdir():
    if i.match('*pedestal.root'):
        files4008.append(i)
dic4008={}
tree_dic4008 = {}
df_dic4008={}
# medianYArraySensor4008=[]
meanYArraySensor4008=[]
meanErrorYArraySensor4008=[]
for x in xArray:
    for i in files4008:
        if i.match('*' + str(x) + '_pedestal*'):
            myfile = i
    dic4008["f{0}".format(x)]=uproot.open(myfile)
    tree_dic4008["treeModule{0}".format(x)]=dic4008["f" + str(x)]["noiseplotter"]["noiseTree"]
    df_dic4008["df_Sensor4008_{0}".format(x)]=tree_dic4008["treeModule" + str(x)].pandas.df()
    # medianYArraySensor4008.append(df_dic4008["df_Sensor4008_{0}".format(x)]["IQR_high"].median())
    meanYArraySensor4008.append(df_dic4008["df_Sensor4008_{0}".format(x)]["RMS_high"].mean())
    meanErrorYArraySensor4008.append(df_dic4008["df_Sensor4008_{0}".format(x)]["RMS_high"].std())
# tree_dic4008["treeModule10"].show()
# print(medianYArraySensor4008)
print(meanYArraySensor4008)

# Module76
p76 = Path('/afs/cern.ch/user/p/psiebere/CMSSW_8_0_1/src/HGCal/voltage_scan/Module76/root_files/') # enter directory here
files76=[]
for i in p76.iterdir():
    if i.match('*pedestal.root'):
        files76.append(i)
dic76={}
tree_dic76 = {}
df_dic76={}
# medianYArrayModule76=[]
meanYArrayModule76=[]
meanErrorYArrayModule76=[]
for x in xArrayModule:
    for i in files76:
        if i.match('*' + str(x) + '_pedestal*'):
            myfile = i
    dic76["f{0}".format(x)]=uproot.open(myfile)
    tree_dic76["treeModule{0}".format(x)]=dic76["f" + str(x)]["noiseplotter"]["noiseTree"]
    df_dic76["df_Module76_{0}".format(x)]=tree_dic76["treeModule" + str(x)].pandas.df()
    # medianYArrayModule76.append(df_dic76["df_Module76_{0}".format(x)]["IQR_high"].median())
    meanYArrayModule76.append(df_dic76["df_Module76_{0}".format(x)]["RMS_high"].mean())
    meanErrorYArrayModule76.append(df_dic76["df_Module76_{0}".format(x)]["RMS_high"].std())
# tree_dic76["treeModule10"].show()
# print(medianYArrayModule76)
print(meanYArrayModule76)

# PPF
f1 = uproot.open("/afs/cern.ch/user/p/psiebere/CMSSW_8_0_1/src/HGCal/voltage_scan/PPF/HPc02_NewGround_ppf_2019-3-27_11-8-45_pedestal.root")
treeModule1 = f1["noiseplotter"]["noiseTree"]
df_ppf = treeModule1.pandas.df()
medianPPF = df_ppf["IQR_high"].mean()
medianYArrayPPF=[]
for i in range(9):
    medianYArrayPPF.append(medianPPF)
print(medianYArrayPPF)

# common plot options
plotmode="mean" #"all", "median", "mean"

font = {# 'family' : 'normal',
        # 'weight' : 'bold',
        'size'   : 22}
matplotlib.rc('font', **font)

# colours
col_red=(255./255, 25./255, 25./255, 1)
col_blue=(75./255, 25./255, 255./255, 1)
col_green=(30./255, 192./255, 5./255, 1)
# print(matplotlib.colors.is_color_like(col_blue))

if (plotmode == "median") or (plotmode == "all"):
    outputpath= '/afs/cern.ch/user/p/psiebere/CMSSW_8_0_1/src/HGCal/voltage_scan/'
    fig1, ax1 = plt.subplots()
    figsize = fig1.get_size_inches()
    fig1.set_size_inches(figsize * 1.5)
    plt.plot(xArray, medianYArraySensor1111)
    plt.plot(xArray, medianYArraySensor3001)
    plt.plot(xArray, medianYArraySensor3008)
    plt.plot(xArray, medianYArraySensor4008)
    plt.plot(xArrayModule, medianYArrayModule76)
    plt.plot(xArray, medianYArrayPPF, linestyle='dashed')
    # ax1.set_title('HighGain (Median used)')
    ax1.set_xlabel("Voltage (V)")
    ax1.set_ylabel("IQR (ADC Counts)")
    plt.legend(["Sensor 1111 (n-type)", "Sensor 3001 (p-type)", "Sensor 3008 (p-type)" ,"Sensor 4008 (p-type)", "Module 76 (n-type)", "floating Pogopins"])
    plt.xlim(0,410)
    plt.ylim(2,10)
    fig1.savefig(outputpath + "voltageScanIQRHG_median_all.png")

    fig2, ax2 = plt.subplots()
    figsize = fig2.get_size_inches()
    fig2.set_size_inches(figsize * 1.5)
    sns.lineplot(x=xArray, y=medianYArraySensor1111)
    # sns.lineplot(x=xArray, y=medianYArraySensor3001)
    # sns.lineplot(x=xArray, y=medianYArraySensor3008)
    # sns.lineplot(x=xArray, y=medianYArraySensor4008)
    sns.lineplot(x=xArrayModule, y=medianYArrayModule76)
    # sns.lineplot(x=xArray, y=medianYArrayPPF, dashes=True)
    ax2.set_title('HighGain (Median used)')
    ax2.set_xlabel("Voltage (V)")
    ax2.set_ylabel("IQR (ADC Counts)")
    plt.legend(["Sensor 1111 (n-type)", "Module 76 (n-type)"])
    plt.xlim(0,410)
    plt.ylim(2,10)
    fig2.savefig(outputpath + "voltageScanIQRHG_median_nType.png")

    fig3, ax3 = plt.subplots()
    figsize = fig3.get_size_inches()
    fig3.set_size_inches(figsize * 1.5)
    # sns.lineplot(x=xArray, y=medianYArraySensor1111)
    sns.lineplot(x=xArray, y=medianYArraySensor3001)
    sns.lineplot(x=xArray, y=medianYArraySensor3008)
    sns.lineplot(x=xArray, y=medianYArraySensor4008)
    # sns.lineplot(x=xArrayModule, y=medianYArrayModule76)
    # sns.lineplot(x=xArray, y=medianYArrayPPF, dashes=True)
    ax3.set_title('HighGain (Median used)')
    ax3.set_xlabel("Voltage (V)")
    ax3.set_ylabel("IQR (ADC Counts)")
    plt.legend(["Sensor 3001 (p-type)", "Sensor 3008 (p-type)", "Sensor 4008 (p-type)"])
    plt.xlim(0,410)
    plt.ylim(2,10)
    fig3.savefig(outputpath + "voltageScanIQRHG_median_pType.png")

if (plotmode == "mean") or (plotmode == "all"):
    outputpath= '/afs/cern.ch/user/p/psiebere/CMSSW_8_0_1/src/HGCal/voltage_scan/'
    fig1, ax1 = plt.subplots()
    figsize = fig1.get_size_inches()
    fig1.set_size_inches(figsize * 1.5)
    plt.errorbar(xArray, meanYArraySensor1111, yerr=meanErrorYArraySensor1111, alpha=0.7, capsize=10)
    plt.errorbar(xArray, meanYArraySensor3001, yerr=meanErrorYArraySensor3001, alpha=0.7, capsize=10)
    plt.errorbar(xArray, meanYArraySensor3008, yerr=meanErrorYArraySensor3008, alpha=0.7, capsize=10)
    plt.errorbar(xArray, meanYArraySensor4008, yerr=meanErrorYArraySensor4008, alpha=0.7, capsize=10)
    plt.errorbar(xArrayModule, meanYArrayModule76, meanErrorYArrayModule76, alpha=0.7, capsize=10)
    plt.plot(xArray, medianYArrayPPF, linestyle='dashed')
    # ax1.set_title('HighGain (Mean used)')
    ax1.set_xlabel("Voltage (V)")
    ax1.set_ylabel("Noise (ADC Counts)")
    plt.legend(["floating pogo pins", "Sensor 1111 (n-type)", "Sensor 3001 (p-type)", "Sensor 3008 (p-type)" ,"Sensor 4008 (p-type)", "Module 76 (n-type)"])
    plt.xlim(0,410)
    # plt.ylim(2,10)
    plt.show()
    fig1.savefig(outputpath + "voltageScanForThesis.png")

if plotmode == None:
    print("Choose plotmode")
