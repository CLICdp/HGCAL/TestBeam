#import ROOT
from  data_frame_loader import data_frame_loader
import uproot
import numpy as np
import pandas as pd
import matplotlib
# matplotlib.use('agg')
import matplotlib.pyplot as plt
from pathlib import Path
from matplotlib.lines import Line2D

cellDF,ChipChannel=data_frame_loader("/afs/cern.ch/user/p/psiebere/CMSSW_8_0_1/src/HGCal/voltage_scan/HPK_6in_135ch_1111_CV.txt","cell")
openDF=data_frame_loader("/afs/cern.ch/user/p/psiebere/CMSSW_8_0_1/src/HGCal/voltage_scan/HPK_6in_135ch_Open_CV.txt","open")

# keys: cell (for silicon cell), channel (skiroc*1000+ch)
voltArray=[50,100,150,200,250,300,350,400]

def get_capacitance_from_channel_and_voltage(skiroc,volt):
    foo_open=openDF[openDF["Voltage"]==volt]
    foo_cell=cellDF[cellDF["Voltage"]==volt]
    pad = int(ChipChannel[ChipChannel.channel==skiroc].cell)
    foo_corr=float(foo_cell[foo_cell["Pad"]==pad].Capacitance)-float(foo_open[foo_open["Pad"]==pad].Capacitance)
    # print("Capacitance = ", foo_corr)
    return(foo_corr)

# print(get_capacitance_from_channel_and_voltage(1036,300))


voltArray=[50,100,150,200,250,300]

# Sensor1111
p1111 = Path('/afs/cern.ch/user/p/psiebere/CMSSW_8_0_1/src/HGCal/voltage_scan/Sensor1111/root_files/') # enter directory here
# p1111 = Path('/afs/cern.ch/user/p/psiebere/CMSSW_8_0_1/src/HGCal/voltage_scan/Module76/root_files/') # enter directory here
files1111=[] #file-list
for i in p1111.iterdir():
    if i.match('*pedestal.root'):
        files1111.append(i)
dic1111={}
tree_dic1111 = {}
df_dic1111={}
# medianYArraySensor1111=[]
meanYArraySensor1111=[]
# meanErrorYArraySensor1111=[]
for x in voltArray:
    for i in files1111:
        if i.match('*' + str(x) + '_pedestal*'):
            myfile = i
    dic1111["f{0}".format(x)]=uproot.open(myfile)
    tree_dic1111["treeModule{0}".format(x)]=dic1111["f" + str(x)]["noiseplotter"]["noiseTree"]
    df_dic1111["df_Sensor1111_{0}".format(x)]=tree_dic1111["treeModule" + str(x)].pandas.df()

listForDF=[]
for ski in range(4):
    for ch in range(0,64,2): #upperleft
        df_row=[ch, ski]
        for x in voltArray:
            df=df_dic1111["df_Sensor1111_{0}".format(x)]
            df_row.append(df[df["skiroc"]==ski][df["channel"]==ch].RMS_high.median())
        listForDF.append(df_row)
noise_df=pd.DataFrame(listForDF, columns=["ch", "ski", "V50", "V100", "V150", "V200", "V250", "V300"])
print(noise_df)


plotmode='Sensor1111' #"all", "c" # needs to match with the input!!!!

font = {# 'family' : 'normal',
        # 'weight' : 'bold',
        'size'   : 22}

matplotlib.rc('font', **font)
cmap = plt.cm.Oranges
kwargs={#
        #"color" : "green",
        }



if (plotmode == "Sensor1111") or (plotmode == "all"):
    outputpath= '/afs/cern.ch/user/p/psiebere/CMSSW_8_0_1/src/HGCal/voltage_scan/'
    fig1, ax1 = plt.subplots()
    figsize = fig1.get_size_inches()
    fig1.set_size_inches(figsize * 1.5)

    # LOWER LEFT, excluded:
    myski=1
    for ch in [58,0,56,38,42,2,4,8,20,6,12]:
        xList=[]
        # currentRow=noise_df[noise_df.ski==myski][noise_df.ch==ch]
        # print("look here: ", currentRow[['ski','ch','V150']])
        yList=noise_df[noise_df["ski"]==myski][noise_df["ch"]==ch][["V50","V100","V150","V200","V250","V300"]].values[0]
        for x in voltArray:
            xList.append(get_capacitance_from_channel_and_voltage(1000*myski+ch,x))
        plt.plot(xList, yList, color='moccasin') #, label=ch)

    # LOWER RIGHT, excluded: 2, 24, 52
    myski=2
    for ch in [18,10,4,0,30,56,58]: #
        xList=[]
        # currentRow=noise_df[noise_df.ski==myski][noise_df.ch==ch]
        # print("look here: ", currentRow[['ski','ch','V150']])
        yList=noise_df[noise_df["ski"]==myski][noise_df["ch"]==ch][["V50","V100","V150","V200","V250","V300"]].values[0]
        for x in voltArray:
            xList.append(get_capacitance_from_channel_and_voltage(1000*myski+ch,x))
        plt.plot(xList, yList, color='orange')

    # UPPER LEFT, excluded:40, 42, 54, 60
    myski=0
    for ch in [36,38,34,20,18,58,0,2,4,16,12]:
        xList=[]
        # currentRow=noise_df[noise_df.ski==myski][noise_df.ch==ch]
        # print("look here: ", currentRow[['ski','ch','V150']])
        yList=noise_df[noise_df["ski"]==myski][noise_df["ch"]==ch][["V50","V100","V150","V200","V250","V300"]].values[0]
        for x in voltArray:
            xList.append(get_capacitance_from_channel_and_voltage(1000*myski+ch,x))
        plt.plot(xList, yList, color='orangered')

    #UPPER RIGHT, excluded:0,4,52
    myski=3
    for ch in [24,14,6,18,16,58,54,56,38,44,50]: #
        xList=[]
        # currentRow=noise_df[noise_df.ski==myski][noise_df.ch==ch]
        # print("look here: ", currentRow[['ski','ch','V150']])
        yList=noise_df[noise_df["ski"]==myski][noise_df["ch"]==ch][["V50","V100","V150","V200","V250","V300"]].values[0]
        for x in voltArray:
            xList.append(get_capacitance_from_channel_and_voltage(1000*myski+ch,x))
        plt.plot(xList, yList, color='darkred')
    # ax1.set_title('HighGain (Mean used)')
    ax1.set_xlabel("Capacitance (pF)")
    ax1.set_ylabel("Noise (ADC Counts)")
    custom_lines = [Line2D([0], [0], color='moccasin', lw=4),
                Line2D([0], [0], color='orange', lw=4),
                Line2D([0], [0], color='orangered', lw=4),
                Line2D([0], [0], color='darkred', lw=4)]
    ax1.legend(custom_lines, ['20um', '40um', '60um','80um'])    # plt.xlim(0,100)
    # plt.ylim(2,10)
    plt.show()
    fig1.savefig(outputpath + "capacitanceScanForThesisSensor1111.png")

if (plotmode == "Module76") or (plotmode == "all"):
    outputpath= '/afs/cern.ch/user/p/psiebere/CMSSW_8_0_1/src/HGCal/voltage_scan/'
    fig1, ax1 = plt.subplots()
    figsize = fig1.get_size_inches()
    fig1.set_size_inches(figsize * 1.5)

    # LOWER LEFT, excluded:
    myski=1
    for ch in [58,0,56,38,42,2,4,8,20,6,12]:
        xList=[]
        # currentRow=noise_df[noise_df.ski==myski][noise_df.ch==ch]
        # print("look here: ", currentRow[['ski','ch','V150']])
        yList=noise_df[noise_df["ski"]==myski][noise_df["ch"]==ch][["V50","V100","V150","V200","V250","V300"]].values[0]
        for x in voltArray:
            xList.append(get_capacitance_from_channel_and_voltage(1000*myski+ch,x))
        plt.plot(xList, yList, color='moccasin') #, label=ch)

    # LOWER RIGHT, excluded:2,24,52
    myski=2
    for ch in [18,10,4,0,30,56,58]: #
        xList=[]
        # currentRow=noise_df[noise_df.ski==myski][noise_df.ch==ch]
        # print("look here: ", currentRow[['ski','ch','V150']])
        yList=noise_df[noise_df["ski"]==myski][noise_df["ch"]==ch][["V50","V100","V150","V200","V250","V300"]].values[0]
        for x in voltArray:
            xList.append(get_capacitance_from_channel_and_voltage(1000*myski+ch,x))
        plt.plot(xList, yList, color='orange')

    # UPPER LEFT, excluded: ,54,60
    myski=0
    for ch in [36,38,34,20,18,58,0,2,4,16,12,40,42]:
        xList=[]
        # currentRow=noise_df[noise_df.ski==myski][noise_df.ch==ch]
        # print("look here: ", currentRow[['ski','ch','V150']])
        yList=noise_df[noise_df["ski"]==myski][noise_df["ch"]==ch][["V50","V100","V150","V200","V250","V300"]].values[0]
        for x in voltArray:
            xList.append(get_capacitance_from_channel_and_voltage(1000*myski+ch,x))
        plt.plot(xList, yList,color='orangered')

    #UPPER RIGHT, excluded:0,4,52
    myski=3
    for ch in [24,14,6,18,16,58,54,56,38,44,50]: #
        xList=[]
        # currentRow=noise_df[noise_df.ski==myski][noise_df.ch==ch]
        # print("look here: ", currentRow[['ski','ch','V150']])
        yList=noise_df[noise_df["ski"]==myski][noise_df["ch"]==ch][["V50","V100","V150","V200","V250","V300"]].values[0]
        for x in voltArray:
            xList.append(get_capacitance_from_channel_and_voltage(1000*myski+ch,x))
        plt.plot(xList, yList, color='darkred')


    # ax1.set_title('HighGain (Mean used)')
    ax1.set_xlabel("Capacitance (pF)")
    ax1.set_ylabel("Noise (ADC Counts)")
    custom_lines = [Line2D([0], [0], color='moccasin', lw=4),
                Line2D([0], [0], color='orange', lw=4),
                Line2D([0], [0], color='orangered', lw=4),
                Line2D([0], [0], color='darkred', lw=4)]
    ax1.legend(custom_lines, ['20um', '40um', '60um','80um'])    # plt.xlim(0,100)
    # plt.ylim(2,10)
    plt.show()
    fig1.savefig(outputpath + "capacitanceScanForThesisModule76.png")

if plotmode == (None or "None"):
    print("Choose plotmode")
