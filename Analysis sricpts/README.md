Requirements: python3, uproot, pandas, numpy, matplotlib

For all plotters, inputfiles are currently hard-coded in the python code.
Inputfile is always the output from CMSSW, with the "fillFullTree"- option set to FALSE.
Additional inputs are mentioned in the list below.

The channels to plot are given in the "skiroc"-format (Chip-Channel(-SCA)) or in the "sensor"-format (1-128).
The matching is stored in the ChannelMappingV3.txt file, which is input for a pandas-dataframe to match both channels where needed.

## interpadplotter.py
- gives boxplots of full-cell-noise for different interpad region
- cells are called in the skiroc-format

## cv_plotter.py
- simple script to plot a CV-curve
- input is a .txt outputfile from the CV scan with ARRAY
- Optional input is an Open-CV file, to do Open correction, if needed. Both inputs are transformed into a Pandas Dataframe
- 'data_frame_loader.py' is used to convert the .txt into a pd.DataFrame()
- cells are called in the skiroc-format


## voltage_scan_plotter.py and voltage_scan_plotter_single_cell.py
- plots Noise vs voltage for the whole sensor (cells summed up with mean) or single cells (multiple cells in one plot)
- Input format: a DIRECTORY with one file per voltage step and files named 'SENSORNAME_VVV_pedestal.root', where SENSORNAME is arbitratry and VVV is the biasvoltage (integer)
- cells are called in the skiroc-format

## voltage_scan_plotter_capacitance.py
- Plot noise vs capacitance, per cell (linear plots)
- same input as voltage_scan
- additional: .txt files from ARRAY CV scan to convert voltage to capacitance (per cell) and ChannelMappingV3.txt to convert input from ARRAY to skiroc-format
- cells are called in the skiroc-format
