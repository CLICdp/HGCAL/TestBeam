from  data_frame_loader import data_frame_loader
import uproot
import numpy as np
import pandas as pd
import matplotlib
# matplotlib.use('agg')
import matplotlib.pyplot as plt
from pathlib import Path
from matplotlib.lines import Line2D

cellDF,ChipChannel=data_frame_loader("/afs/cern.ch/user/p/psiebere/CMSSW_8_0_1/src/HGCal/voltage_scan/HPK_6in_135ch_1111_CV.txt","cell")
openDF=data_frame_loader("/afs/cern.ch/user/p/psiebere/CMSSW_8_0_1/src/HGCal/voltage_scan/HPK_6in_135ch_Open_CV.txt","open")

voltArray=[25,50,75,100,125,150,170,180,190,200,210,220,230,250,300]
cArray=[]
for volt in voltArray:
    foo_open=openDF[openDF["Voltage"]==volt][openDF["Pad"]==19]
    foo_cell=cellDF[cellDF["Voltage"]==volt][openDF["Pad"]==19]
    foo_corr=float(foo_cell.Capacitance)-float(foo_open.Capacitance)
    cArray.append(foo_corr)
# print(cArray)

plotmode='Sensor1111' #"all", "c" #

font = {# 'family' : 'normal',
        # 'weight' : 'bold',
        'size'   : 22}

matplotlib.rc('font', **font)
kwargs={#
        #"color" : "green",
        }

if (plotmode == "Sensor1111") or (plotmode == "all"):
    outputpath= '/afs/cern.ch/user/p/psiebere/CMSSW_8_0_1/src/HGCal/voltage_scan/'
    fig1, ax1 = plt.subplots()
    figsize = fig1.get_size_inches()
    fig1.set_size_inches(figsize * 1.5)
    plt.plot(voltArray, cArray, 'k') #, label=ch)
    plt.plot(voltArray, cArray, 'ro') #, label=ch)
    # ax1.set_title('HighGain (Mean used)')
    ax1.set_xlabel("Voltage (V)")
    ax1.set_ylabel("Capacitance (pF)")
    ax1.legend(['Sensor1111'])
    # plt.xlim(0,100)
    # plt.ylim(2,10)
    plt.show()
    fig1.savefig(outputpath + "capacitanceVSvoltageSensor1111Cell19.png")

if plotmode == (None or "None"):
    print("Choose plotmode")
