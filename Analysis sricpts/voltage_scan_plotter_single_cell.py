#import ROOT
import uproot
import numpy as np
#import pandas as pd
import matplotlib
# matplotlib.use('agg')
import matplotlib.pyplot as plt
import seaborn as sns #; sns.set()
sns.set_context("talk")
#import csv
from pathlib import Path

xArray=[10,50,100,150,200,250,300,350,400]
xArrayModule=[10,50,100,150,200,250,300,350]

# Sensor4008
p4008 = Path('/afs/cern.ch/user/p/psiebere/CMSSW_8_0_1/src/HGCal/voltage_scan/Sensor3008/root_files/') # enter directory here
files4008=[]
for i in p4008.iterdir():
    if i.match('*pedestal.root'):
        files4008.append(i)
dic4008={}
tree_dic4008 = {}
df_dic4008={}
# medianYArraySensor4008=[]
ArraySensor4008={}
for ch in range(0,416,13):
    tmpArray=[]
    for x in xArray:
        for i in files4008:
            if i.match('*' + str(x) + '_pedestal*'):
                myfile = i
        dic4008["f{0}".format(x)]=uproot.open(myfile)
        tree_dic4008["treeModule{0}".format(x)]=dic4008["f" + str(x)]["noiseplotter"]["noiseTree"]
        df_dic4008["df_Sensor4008_{0}".format(x)]=tree_dic4008["treeModule" + str(x)].pandas.df()
        # medianYArraySensor4008.append(df_dic4008["df_Sensor4008_{0}".format(x)]["IQR_high"].median())
        tmpArray.append(df_dic4008["df_Sensor4008_{0}".format(x)][df_dic4008["df_Sensor4008_{0}".format(x)].skiroc==0][df_dic4008["df_Sensor4008_{0}".format(x)].sca==0]["RMS_high"][ch])
    print("tmpArray: ", tmpArray)
    ArraySensor4008["ch{0}".format(ch)]=tmpArray
    # tree_dic4008["treeModule10"].show()

# PPF
f1 = uproot.open("/afs/cern.ch/user/p/psiebere/CMSSW_8_0_1/src/HGCal/voltage_scan/PPF/HPc02_NewGround_ppf_2019-3-27_11-8-45_pedestal.root")
treeModule1 = f1["noiseplotter"]["noiseTree"]
df_ppf = treeModule1.pandas.df()
medianPPF = df_ppf["IQR_high"].median()
medianYArrayPPF=[]
for i in range(9):
    medianYArrayPPF.append(medianPPF)
print(medianYArrayPPF)

# common plot options
binmode=None
my_kde=False
plotmode="mean" #"all", "median", "mean"

# colours
col_red=(255./255, 25./255, 25./255, 1)
col_blue=(75./255, 25./255, 255./255, 1)
col_green=(30./255, 192./255, 5./255, 1)
# print(matplotlib.colors.is_color_like(col_blue))

if (plotmode == "mean") or (plotmode == "all"):
    outputpath= '/afs/cern.ch/user/p/psiebere/CMSSW_8_0_1/src/HGCal/voltage_scan/'
    fig1, ax1 = plt.subplots()
    figsize = fig1.get_size_inches()
    fig1.set_size_inches(figsize * 1.5)
    for ch in range(0,416,13):
        plt.plot(xArray, ArraySensor4008["ch{0}".format(ch)], alpha=0.7)
    plt.plot(xArray, medianYArrayPPF, linestyle='dashed')
    ax1.set_xlabel("Voltage (V)")
    ax1.set_ylabel("IQR (ADC Counts)")
    ax1.set_title("Sensor4008, Chip 0, all 32 Channels")
    # plt.legend(["Sensor 1111 (n-type)", "Sensor 3001 (p-type)", "Sensor 3008 (p-type)" ,"Sensor 4008 (p-type)", "Module 76 (n-type)", "floating Pogopins"])
    # plt.xlim(0,410)
    # plt.ylim(2,10)
    plt.show()
    fig1.savefig(outputpath + "voltageScan3008.png")

if plotmode == None:
    print("Choose plotmode")
