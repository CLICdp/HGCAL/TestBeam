import pandas as pd

mapping = open("/afs/cern.ch/user/p/psiebere/CMSSW_8_0_1/src/HGCal/voltage_scan/ChannelMappingV3.txt","r")
filelines = mapping.readlines()
ChipChannel = []
for x in filelines[1:]:
    x = x.split()
    if x[1] == "notConnected":
        ChipChannel.append([int(x[0]),x[1]])
    else:
        ChipChannel.append([int(x[0]),int(x[1])])
ChannelDF = pd.DataFrame(ChipChannel, columns=['cell','channel'])

def data_frame_loader(file, mode):
    if mode == "cell":
        f1 = open(file,"r")
        filelines = f1.readlines()
        #Voltage	#Channel	#Capacitance	#Tot. curr.	#Act. vlt.	#Time	#Temp	#Humid.	#C_serial	#Impedance	#Phase
        cellList=[]
        for x in filelines[21:]:
            x = x.split()
            for i in range(11):
                x[i] = float(x[i])
            for i in [0,1,4]:
                x[i] = int(x[i])
            cellList.append(x)
        f1.close()
        cellDF = pd.DataFrame(cellList, columns=['Voltage','Pad','Capacitance','Total_current','Actual_voltage','Time','Temperature','Humidity','Serial_Capacitance','Impedance','Phase'])
        # print(cellList)
        del cellList
        # print(cellDF)
        return(cellDF,ChannelDF)


    if mode == "open":
        f1 = open(file,"r")
        filelines = f1.readlines()
        #Voltage	#Channel	#Capacitance	#Tot. curr.	#Act. vlt.	#Time	#Temp	#Humid.	#C_serial	#Impedance	#Phase
        cellList=[]
        for x in filelines[29:]:
            x = x.split()
            for i in range(17):
                x[i] = float(x[i])
            for i in [0,1,5]:
                x[i] = int(x[i])
            cellList.append(x)
        f1.close()
        cellDF = pd.DataFrame(cellList, columns=['Voltage','Pad','Capacitance','Error','Total_current','Actual_voltage','Time','Temperature','Humidity','Serial_Capacitance','Error2','Impedance','Error3','Phase','Error4','Cp uncoor.','Cs uncorr.'])
        del cellList
        # print(cellDF)
        return(cellDF)
