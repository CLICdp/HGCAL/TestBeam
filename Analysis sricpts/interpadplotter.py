#import ROOT
import uproot
import numpy as np
import pandas as pd
import matplotlib
# matplotlib.use('agg')
import matplotlib.pyplot as plt
import seaborn as sns #; sns.set()
# sns.set_context("talk")
#import csv
from pathlib import Path

xArray=[10,50,100,150,200,250,300,350,400]

# Sensor1111
f1 = uproot.open('/afs/cern.ch/user/p/psiebere/CMSSW_8_0_1/src/HGCal/interpad/root_files/sensor1111.root')
treeModule1 = f1["noiseplotter"]["noiseTree"]
df_Sensor1111 = treeModule1.pandas.df()
upperLeft= np.array([])
upperRight=np.array([])
lowerLeft=np.array([])
lowerRight=np.array([])


for ch in [36,38,34,54,40,20,18,42,60,58,0,2,4,16,12]:
    foo=(df_Sensor1111[df_Sensor1111.skiroc==0][df_Sensor1111.channel==ch].IQR_high).to_numpy()
    for x in foo :
        if x<10 :
            upperLeft = np.append(upperLeft, x)

for ch in [24,14,6,18,16,0,4,58,54,56,38,52,44,50]:
    foo=(df_Sensor1111[df_Sensor1111.skiroc==3][df_Sensor1111.channel==ch].IQR_high).to_numpy()
    for x in foo :
        if x<10 :
            upperRight = np.append(upperRight, x)

for ch in [58,0,56,38,42,2,4,8,20,6,12]:
    foo = (df_Sensor1111[df_Sensor1111.skiroc==1][df_Sensor1111.channel==ch].IQR_high).to_numpy()
    for x in foo :
        if x<10 :
            lowerLeft = np.append(lowerLeft, x)

for ch in [24,2,18,10,4,0,30,56,58,52]:
    foo = (df_Sensor1111[df_Sensor1111.skiroc==2][df_Sensor1111.channel==ch].IQR_high).to_numpy()
    for x in foo:
        if (x < 10):
            lowerRight = np.append(lowerRight, x)

# delete all possible nan
upperLeft = upperLeft[np.logical_not(np.isnan(upperLeft))]
upperRight = upperRight[np.logical_not(np.isnan(upperRight))]
lowerLeft = lowerLeft[np.logical_not(np.isnan(lowerLeft))]
lowerRight = lowerRight[np.logical_not(np.isnan(lowerRight))]

print(len(upperLeft), len(upperRight), len(lowerLeft), len(lowerRight))

# print(upperRight)

# outputData = {'20um':lowerLeft, '40um':lowerRight, '60um': upperLeft, '80um':upperRight, }

kwargs={'widths':0.5, 'whis':1, 'patch_artist':True ,'flierprops':{'marker':'D', 'markerfacecolor':'red'}}

plotmode="interpad" #"all", "interpad"

# https://matplotlib.org/3.1.0/gallery/statistics/boxplot_color.html <<<<<<<<<<<<<<<plot like this!!!!

font = {# 'family' : 'normal',
        # 'weight' : 'bold',
        'size'   : 22}
matplotlib.rc('font', **font)

if (plotmode == "interpad") or (plotmode == "all"):
    outputpath= '/afs/cern.ch/user/p/psiebere/CMSSW_8_0_1/src/HGCal/interpad/'
    labels = ['20', '40', '60','80']
    fig1, ax1 = plt.subplots()
    figsize = fig1.get_size_inches()
    fig1.set_size_inches(figsize * 1.5)
    # figsize = fig1.get_size_inches()
    # fig1.set_size_inches(figsize * 1.5)
    # ax1.set_title('Noise vs Interpad')
    ax1.set_ylabel("Noise (ADC-Counts)")
    ax1.set_xlabel("Interpad Distance (um)")
    bplot1 = plt.boxplot(x=[lowerLeft, lowerRight, upperLeft, upperRight], labels=labels, **kwargs)
    # ax1.set_xlabel("Interpad Capacitance (um)")
    # plt.legend(["20um", "40um", "60um" ,"80um"]
    # plt.xlim(0,5)
    # plt.ylim(4,8)
    colors = ['pink', 'lightblue', 'lightgreen', 'lightyellow']
    for patch, color in zip(bplot1["boxes"], colors):
        patch.set_facecolor(color)
    plt.show()
    fig1.savefig(outputpath + "interpad_sensor1111_forThesis.png")




if plotmode == None:
    print("Choose plotmode")
