#include <iostream>
#include "TH1F.h"
#include "TH2F.h"
#include "TH2Poly.h"
#include "TTree.h"
#include "stdlib.h"
#include <fstream>
#include <sstream>
// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "HGCal/DataFormats/interface/HGCalTBSkiroc2CMSCollection.h"
#include "HGCal/DataFormats/interface/HGCalTBDetId.h"
#include "CommonTools/UtilAlgos/interface/TFileService.h"
#include "HGCal/CondObjects/interface/HGCalElectronicsMap.h"
#include "HGCal/CondObjects/interface/HGCalCondObjectTextIO.h"
#include "HGCal/DataFormats/interface/HGCalTBElectronicsId.h"
#include "HGCal/Geometry/interface/HGCalTBCellVertices.h"
#include "HGCal/Geometry/interface/HGCalTBTopology.h"
#include "HGCal/Geometry/interface/HGCalTBGeometryParameters.h"
#include "HGCal/CondObjects/interface/HGCalTBDetectorLayout.h"
#include "HGCal/DataFormats/interface/HGCalTBRunData.h" //for the runData type definition
#include <iomanip>
#include <set>

//#define DEBUG

struct hgcal_capacitor{
  hgcal_capacitor() : key(0),
		    medianHG(0.),
		    medianLG(0.),
		    iqrHG(0.),
		    iqrLG(0.),
        meanHG(0.),
        meanLG(0.),
        rmsHG(0.),
        rmsLG(0.){;}
  int key;
  float medianHG;
  float medianLG;
  float iqrHG;
  float iqrLG;
  float meanHG;
  float meanLG;
  float rmsHG;
  float rmsLG;
  std::vector<float> highGain; //key - value = 1000*board + ... +SCA - ADCValueArray
  std::vector<float> lowGain; //key - value = 1000*board + ... +SCA - ADCValueArray
};

struct pedestalChannel{
    HGCalTBDetId id;
    float pedHGMean[NUMBER_OF_SCA];
    float pedLGMean[NUMBER_OF_SCA];
    float pedHGIQR[NUMBER_OF_SCA];
    float pedLGIQR[NUMBER_OF_SCA];
};


class NoisePlotter : public edm::one::EDAnalyzer<edm::one::SharedResources>
{
public:
  explicit NoisePlotter(const edm::ParameterSet&);
  ~NoisePlotter();
  static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);
private:
  virtual void beginJob() override;
  void analyze(const edm::Event& , const edm::EventSetup&) override;
  virtual void endJob() override;
  void InitTH2Poly(TH2Poly& poly, int layerID, int sensorIU, int sensorIV);

  struct {
    HGCalElectronicsMap emap_;
    HGCalTBDetectorLayout layout_;
  } essource_;
  int m_sensorsize;

  bool m_writePedestalFile;
  std::string m_pedestalHigh_filename;
  std::string m_pedestalLow_filename;
  bool m_writeNoisyChannelFile;
  std::string m_noisyChannels_filename;
  bool m_writeTreeOutput;
  std::string m_electronicMap;
  std::string m_detectorLayoutFile;
  int m_NTSForPedestalComputation;
  bool m_fillFullTree;
  bool m_useMeanForCM;


  int m_evtID;
  uint16_t m_numberOfBoards;
  std::map<int,hgcal_capacitor> m_channelMap;

  edm::EDGetTokenT<HGCalTBSkiroc2CMSCollection> m_HGCalTBSkiroc2CMSCollection;


  HGCalTBTopology IsCellValid;
  HGCalTBCellVertices TheCell;
  std::vector<std::pair<double, double>> CellXY;
  std::pair<double, double> CellCentreXY;
  std::set< std::pair<int,HGCalTBDetId> > setOfConnectedDetId;

  std::map<int,pedestalChannel> m_pedMap; //key=10000*hexa+100*chip+chan

};

NoisePlotter::NoisePlotter(const edm::ParameterSet& iConfig) : //constructor
  m_sensorsize(iConfig.getUntrackedParameter<int>("SensorSize",128)),
  m_writePedestalFile(iConfig.getUntrackedParameter<bool>("WritePedestalFile",false)),
  m_pedestalHigh_filename( iConfig.getUntrackedParameter<std::string>("HighGainPedestalFileName",std::string("pedestalHG.txt")) ),
  m_pedestalLow_filename( iConfig.getUntrackedParameter<std::string>("LowGainPedestalFileName",std::string("pedestalLG.txt")) ),
  m_writeNoisyChannelFile(iConfig.getUntrackedParameter<bool>("WriteNoisyChannelsFile",false)),
  m_noisyChannels_filename( iConfig.getUntrackedParameter<std::string>("NoisyChannelsFileName",std::string("noisyChannels.txt")) ),
  m_writeTreeOutput(iConfig.getUntrackedParameter<bool>("WriteTreeOutput",false)),
  m_electronicMap(iConfig.getUntrackedParameter<std::string>("ElectronicMap","HGCal/CondObjects/data/map_CERN_Hexaboard_28Layers.txt")),
  m_detectorLayoutFile(iConfig.getUntrackedParameter<std::string>("DetectorLayout", "HGCal/CondObjects/data/layerGeom_oct2017_h2_17layers.txt")),
  m_NTSForPedestalComputation(iConfig.getUntrackedParameter<int>("NTSForPedestalComputation",1)),
  m_fillFullTree(iConfig.getUntrackedParameter<bool>("fillFullTree",false)),
  m_useMeanForCM(iConfig.getUntrackedParameter<bool>("useMeanForCM",false))
{
  m_HGCalTBSkiroc2CMSCollection = consumes<HGCalTBSkiroc2CMSCollection>(iConfig.getParameter<edm::InputTag>("InputCollection"));

  m_evtID=0;

  HGCalCondObjectTextIO io(0);
  edm::FileInPath fip(m_electronicMap);
  if (!io.load(fip.fullPath(), essource_.emap_)) {
    throw cms::Exception("Unable to load electronics map");
  };
 fip = edm::FileInPath(m_detectorLayoutFile);
  if (!io.load(fip.fullPath(), essource_.layout_)) {
    throw cms::Exception("Unable to load detector layout file");
  };
  std::cout << iConfig.dump() << std::endl;
}


NoisePlotter::~NoisePlotter() // Destructor
{

}

void NoisePlotter::analyze(const edm::Event& event, const edm::EventSetup& setup)
{
  edm::Handle<HGCalTBSkiroc2CMSCollection> skirocs;
  event.getByToken(m_HGCalTBSkiroc2CMSCollection, skirocs);

  HGCalTBTopology topo;

  m_numberOfBoards = skirocs->size()/HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA;

  m_evtID++;

  if( !skirocs->size() ) return;

  std::vector<float> cmHG;
  std::vector<float> cmLG;
  std::vector<float> cmHGHalf;
  std::vector<float> cmLGHalf;
  for( size_t iski=0;iski<skirocs->size(); iski++ ){ //loop over boards
    if(iski%HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA!=0) continue;
    int iboard=iski/HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA;
    for( size_t it=0; it<NUMBER_OF_SCA; it++ ){
      std::vector<float> cmMapHG;
      std::vector<float> cmMapLG;
      std::vector<float> cmMapHGHalf;
      std::vector<float> cmMapLGHalf;
      for( size_t iski2=0;iski2<4; iski2++ ){
	int iski3=iboard*HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA+iski2;
	HGCalTBSkiroc2CMS skiroc=skirocs->at(iski3);
	std::vector<int> rollpositions=skiroc.rollPositions();
	int skiId=HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA*(iboard)+(HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA-iski2)%HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA+1;
	for( size_t ichan=0; ichan<HGCAL_TB_GEOMETRY::N_CHANNELS_PER_SKIROC; ichan++ ){
	  if(ichan%2!=0) continue;
	  HGCalTBDetId detid2(-1);
	  HGCalTBElectronicsId eid(skiId,ichan);
	  if (essource_.emap_.existsEId(eid.rawId())) {
	    detid2 = essource_.emap_.eid2detId(eid);
	    HGCalTBLayer layer = essource_.layout_.at(detid2.layer() - 1);
	  }
	  else{
	    HGCalTBDetId did(-1);
	    detid2=did;
	  }
	  //if( rollpositions[it]>0) continue; //Makes sure iterator is at TimeSample 0
    int peak=(it-rollpositions[it]+2)%NUMBER_OF_SCA;
	  if(skiroc.ADCHigh(ichan,peak)-m_pedMap[ iboard*10000+(iski3%HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA)*100+ichan].pedHGMean[peak]>100) continue; //if ADC - ADCpeak > 100 -> cuts off very noisy channels
    if(detid2.cellType()==0) { //FullCells
	    cmMapHG.push_back(skiroc.ADCHigh(ichan,it)-m_pedMap[ iboard*10000+(iski3%HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA)*100+ichan].pedHGMean[it]); //assigns the roughly 800 values per capacitor
	    cmMapLG.push_back(skiroc.ADCLow(ichan,it)-m_pedMap[ iboard*10000+(iski3%HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA)*100+ichan].pedLGMean[it]);
	  }
    if(detid2.cellType()==2) { //HalfCells
	    cmMapHGHalf.push_back(skiroc.ADCHigh(ichan,it)-m_pedMap[ iboard*10000+(iski3%HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA)*100+ichan].pedHGMean[it]); //assigns the roughly 800 values per capacitor
	    cmMapLGHalf.push_back(skiroc.ADCLow(ichan,it)-m_pedMap[ iboard*10000+(iski3%HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA)*100+ichan].pedLGMean[it]);
	  }
	}
      }
    //
   //Full cells //calculates the median of the 800 entries which is the CMN for each capacitor
       if(cmMapHG.size()==0) continue;
       sort(cmMapHG.begin(),cmMapHG.end());
       sort(cmMapLG.begin(),cmMapLG.end());
       int medianIndex = int( 0.5*(cmMapHG.size()-1) );
       float cmHigh= 0.5*(cmMapHG[medianIndex]+cmMapHG[medianIndex+1]);
       if (m_useMeanForCM){ //overwrite in case
         cmHigh= std::accumulate(cmMapHG.begin(), cmMapHG.end(), 0.0) /cmMapHG.size();
       }
       medianIndex = int( 0.5*(cmMapLG.size()-1) );
       float cmLow= 0.5*(cmMapLG[medianIndex]+cmMapLG[medianIndex+1]);
       if (m_useMeanForCM){ //overwrite in case
         cmLow= std::accumulate(cmMapLG.begin(), cmMapLG.end(), 0.0) /cmMapLG.size();
       }
      cmHG.push_back(cmHigh); //the board*13 values are pushed back without key -> only the order of the vector let's us trace back with value is which TS it is
      cmLG.push_back(cmLow);  // begins with board0 SCA0, board0 SCA1 etc...
      // // -------------- for printing the CM-----
      // std::ostringstream vts;
      // if (!cmHG.empty())
      // {
      //   // Convert all but the last element to avoid a trailing ","
      //   std::copy(cmHG.begin(), cmHG.end()-1,
      //       std::ostream_iterator<int>(vts, ", "));
      //   // Now add the last element with no delimiter
      //   vts << cmHG.back();
      // }
      // std::cout << "look here:" << vts.str() << std::endl;
      // // --------------
      cmMapHG.clear();
      cmMapLG.clear();
      //HalfCells cells //calculates the median of the 800 entries which is the CMN for each capacitor
      if(cmMapHGHalf.size()==0) continue;
      sort(cmMapHGHalf.begin(),cmMapHGHalf.end());
      sort(cmMapLGHalf.begin(),cmMapLGHalf.end());
      medianIndex = int( 0.5*(cmMapHGHalf.size()-1) );
      cmHigh= 0.5*(cmMapHGHalf[medianIndex]+cmMapHGHalf[medianIndex+1]);
      if (m_useMeanForCM){ //overwrite in case
        cmHigh= std::accumulate(cmMapHGHalf.begin(), cmMapHGHalf.end(), 0.0) /cmMapHGHalf.size();
      }
      medianIndex = int( 0.5*(cmMapLGHalf.size()-1) );
      cmLow= 0.5*(cmMapLGHalf[medianIndex]+cmMapLGHalf[medianIndex+1]);
      if (m_useMeanForCM){ //overwrite in case
        cmLow= std::accumulate(cmMapLGHalf.begin(), cmMapLGHalf.end(), 0.0) /cmMapLGHalf.size();
      }
     cmHGHalf.push_back(cmHigh); //the board*13 values are pushed back without key -> only the order of the vector let's us trace back with value is which TS it is
     cmLGHalf.push_back(cmLow);  // begins with board0 SCA0, board0 SCA1 etc...
     // // -------------- for printing the CM-----
     // std::ostringstream vts;
     // if (!cmHG.empty())
     // {
     //   // Convert all but the last element to avoid a trailing ","
     //   std::copy(cmHG.begin(), cmHG.end()-1,
     //       std::ostream_iterator<int>(vts, ", "));
     //   // Now add the last element with no delimiter
     //   vts << cmHG.back();
     // }
     // std::cout << "look here:" << vts.str() << std::endl;
     // // --------------
     cmMapHGHalf.clear();
     cmMapLGHalf.clear();
    }
  } // all struct hgcal_capacitors created and the highgain / lowgain vectors are filled (but no Tree...)
    // ###################################################################################### all loops done here #############


  for( size_t iski=0;iski<skirocs->size(); iski++ ){//loop over boards
    HGCalTBSkiroc2CMS skiroc=skirocs->at(iski);
    //std::cout << skiroc << std::endl;
    std::vector<int> rollpositions=skiroc.rollPositions();
    int iboard=iski/HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA;
    for( size_t ichan=0; ichan<HGCAL_TB_GEOMETRY::N_CHANNELS_PER_SKIROC; ichan++ ){
      HGCalTBDetId detid=skiroc.detid( ichan );
      HGCalTBElectronicsId eid( essource_.emap_.detId2eid(detid.rawId()) );
      if( essource_.emap_.existsEId(eid) ){
	std::pair<int,HGCalTBDetId> p( iboard*1000+(iski%HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA)*100+ichan,skiroc.detid(ichan) );
	setOfConnectedDetId.insert(p);
      }

      else continue;
      HGCalTBDetId detid2(-1);
      int skiId=HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA*(iboard)+(HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA-iski)%HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA+1;
      HGCalTBElectronicsId eid2(skiId,ichan);
      //    int  moduleId=-1;
      if (essource_.emap_.existsEId(eid2.rawId())) {
	detid2 = essource_.emap_.eid2detId(eid2);
	HGCalTBLayer layer = essource_.layout_.at(detid2.layer() - 1);
	//	moduleId = layer.at( detid2.sensorIU(), detid2.sensorIV() ).moduleID();
      }
      else{
	HGCalTBDetId did(-1);
	detid2=did;
      }
      int celltype=detid2.cellType();
      for( size_t it=0; it<NUMBER_OF_SCA; it++ ){
  // The following loop overwrite highGain in the structs with (meas.ADC - Pedestal - CM). The else covers the SCA where TS was 0, the if alls the others
	if( rollpositions[it]<m_NTSForPedestalComputation ){ //consider only a certain number of time samples for pedestal subtraction //skiroc.rollpositions is SCA->TS
	  uint32_t key=iboard*100000+(iski%HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA)*10000+ichan*100+it;
	  std::map<int,hgcal_capacitor>::iterator iter=m_channelMap.find(key);
	  if( iter==m_channelMap.end() ){ //checking is vector is empty - if yes, create it.
	    hgcal_capacitor tmp;
	    tmp.key=key;
	    std::vector<float> vecH,vecL; //
      // if(celltype==2){ //full cells -> real!
      if(0){ //half cells -> real!
  	    vecH.push_back(skiroc.ADCHigh(ichan,it)-cmHGHalf[iboard*13+it] - m_pedMap[iboard*10000+(iski%HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA)*100+ichan].pedHGMean[it]);
  	    vecL.push_back(skiroc.ADCLow(ichan,it)-cmLGHalf[iboard*13+it] - m_pedMap[iboard*10000+(iski%HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA)*100+ichan].pedLGMean[it]);
        tmp.highGain=vecH;
  	    tmp.lowGain=vecL;
  	    std::pair<int,hgcal_capacitor> p(key,tmp);
  	    m_channelMap.insert( p );
      }
      else{
  	    vecH.push_back(skiroc.ADCHigh(ichan,it)-cmHG[iboard*13+it]*topo.Cell_Area(celltype) / topo.Cell_Area(0) - m_pedMap[iboard*10000+(iski%HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA)*100+ichan].pedHGMean[it]);
        // vecH.push_back(skiroc.ADCHigh(ichan,it)- m_pedMap[iboard*10000+(iski%HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA)*100+ichan].pedHGMean[it]);
        // vecH.push_back(skiroc.ADCHigh(ichan,it));
  	    vecL.push_back(skiroc.ADCLow(ichan,it)-cmLG[iboard*13+it]*topo.Cell_Area(celltype) / topo.Cell_Area(0) - m_pedMap[iboard*10000+(iski%HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA)*100+ichan].pedLGMean[it]);
        // vecL.push_back(skiroc.ADCLow(ichan,it)- m_pedMap[iboard*10000+(iski%HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA)*100+ichan].pedLGMean[it]);
        // vecL.push_back(skiroc.ADCLow(ichan,it));
  	    tmp.highGain=vecH;
  	    tmp.lowGain=vecL;
  	    std::pair<int,hgcal_capacitor> p(key,tmp);
  	    m_channelMap.insert( p );
      }
	  }
	  else{ //belongs to second if
      // if(celltype==2){ //full cells -> real!
      if(0){ //half cells -> real!
  	    iter->second.highGain.push_back(skiroc.ADCHigh(ichan,it)-cmHGHalf[iboard*13+it] - m_pedMap[iboard*10000+(iski%HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA)*100+ichan].pedHGMean[it]);
        iter->second.lowGain.push_back(skiroc.ADCLow(ichan,it)-cmLGHalf[iboard*13+it] - m_pedMap[iboard*10000+(iski%HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA)*100+ichan].pedLGMean[it]);
        {float dummy;
        dummy = topo.Cell_Area(celltype);
        dummy = dummy;
        }
      }
      else{
  	    iter->second.highGain.push_back(skiroc.ADCHigh(ichan,it)-cmHG[iboard*13+it]*(topo.Cell_Area(celltype) / topo.Cell_Area(0)) - m_pedMap[iboard*10000+(iski%HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA)*100+ichan].pedHGMean[it]);
        // iter->second.highGain.push_back(skiroc.ADCHigh(ichan,it)- m_pedMap[iboard*10000+(iski%HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA)*100+ichan].pedHGMean[it]);
        // iter->second.highGain.push_back(skiroc.ADCHigh(ichan,it));
        iter->second.lowGain.push_back(skiroc.ADCLow(ichan,it)-cmLG[iboard*13+it]*(topo.Cell_Area(celltype) / topo.Cell_Area(0)) - m_pedMap[iboard*10000+(iski%HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA)*100+ichan].pedLGMean[it]);
        // iter->second.lowGain.push_back(skiroc.ADCLow(ichan,it)- m_pedMap[iboard*10000+(iski%HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA)*100+ichan].pedLGMean[it]);
        // iter->second.lowGain.push_back(skiroc.ADCLow(ichan,it));
      }
    }
	}
      }
    }
  } // all struct hgcal_capacitors created and the highgain / lowgain vectors are filled with the "NEW" ADC value (but no Tree...)

}

void NoisePlotter::InitTH2Poly(TH2Poly& poly, int layerID, int sensorIU, int sensorIV)
{
  double HexX[HGCAL_TB_GEOMETRY::MAXVERTICES] = {0.};
  double HexY[HGCAL_TB_GEOMETRY::MAXVERTICES] = {0.};
  for(int iv = -7; iv < 8; iv++) {
    for(int iu = -7; iu < 8; iu++) {
      if(!IsCellValid.iu_iv_valid(layerID, sensorIU, sensorIV, iu, iv, m_sensorsize)) continue;
      CellXY = TheCell.GetCellCoordinatesForPlots(layerID, sensorIU, sensorIV, iu, iv, m_sensorsize);
      assert(CellXY.size() == 4 || CellXY.size() == 6);
      unsigned int iVertex = 0;
      for(std::vector<std::pair<double, double>>::const_iterator it = CellXY.begin(); it != CellXY.end(); it++) {
	HexX[iVertex] =  it->first;
	HexY[iVertex] =  it->second;
	++iVertex;
      }
      //Somehow cloning of the TH2Poly was not working. Need to look at it. Currently physically booked another one.
      poly.AddBin(CellXY.size(), HexX, HexY);
    }//loop over iu
  }//loop over iv
}

void NoisePlotter::beginJob()
{
   FILE* file;
    char buffer[300];
    file = fopen (m_pedestalHigh_filename.c_str() , "r"); //low HG file for assigning each key the pedestal value
    if (file == NULL){ perror ("Error opening pedestal high gain"); exit(1); }
    else{
      while ( ! feof (file) ){
	if ( fgets (buffer , 300 , file) == NULL ) break;
	pedestalChannel ped;
	const char* index = buffer;
	int hexaboard,skiroc,channel,ptr,nval;
	nval=sscanf( index, "%d %d %d %n",&hexaboard,&skiroc,&channel,&ptr );
	//	std::cout<<"Filling "<<hexaboard<<" "<<skiroc<<" "<<channel<<std::endl;
	//	std::cout<<"NVal "<<nval<<std::endl;
	if( nval==3 ){
	  int skiId=HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA*hexaboard+(HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA-skiroc)%HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA+1;
	  HGCalTBElectronicsId eid(skiId,channel);
	  if (!essource_.emap_.existsEId(eid.rawId())){
	    ped.id = HGCalTBDetId(-1);
	  }
	  else{
	    ped.id = essource_.emap_.eid2detId(eid);
	  }
	  index+=ptr;
	}else continue;
	for( unsigned int ii=0; ii<NUMBER_OF_SCA; ii++ ){
	  float mean,iqr;
	  nval = sscanf( index, "%f %f %n",&mean,&iqr,&ptr );
	  //	  std::cout<<"Values for SCA "<<ii<<" : "<<mean<<" "<<iqr<<std::endl;
	  if( nval==2 ){
	    ped.pedHGMean[ii]=mean;
	    ped.pedHGIQR[ii]=iqr;
	    index+=ptr;
	  }else continue;
	}
	//	std::cout<<"Number "<<10000*hexaboard+100*skiroc+channel<<std::endl;
     	m_pedMap.insert( std::pair<int,pedestalChannel>(10000*hexaboard+100*skiroc+channel,ped) );
      }
      fclose (file);
    }
   file = fopen (m_pedestalLow_filename.c_str() , "r"); //low LG file for assigning each key the pedestal value
    if (file == NULL){ perror ("Error opening pedestal low gain"); exit(1); }
    else{
      while ( ! feof (file) ){
	if ( fgets (buffer , 300 , file) == NULL ) break;
	pedestalChannel ped;
	const char* index = buffer;
	int hexaboard,skiroc,channel,ptr,nval,key;
	nval=sscanf( index, "%d %d %d %n",&hexaboard,&skiroc,&channel,&ptr );
	if( nval==3 ){
	  key=10000*hexaboard+100*skiroc+channel;
	  index+=ptr;
	}else continue;
	for( unsigned int ii=0; ii<NUMBER_OF_SCA; ii++ ){
	  float mean,iqr;
	  nval = sscanf( index, "%f %f %n",&mean,&iqr,&ptr );
	  if( nval==2 ){
	    m_pedMap[key].pedLGMean[ii]=mean;
	    m_pedMap[key].pedLGIQR[ii]=iqr;
	    index+=ptr;
	  }else continue;
	}
      }
      fclose (file);
    }
}

void NoisePlotter::endJob() //creates output directories in TFile
{
  usesResource("TFileService");
  edm::Service<TFileService> fs;
  std::map<int,TH2Poly*>  hgMedianMap;
  std::map<int,TH2Poly*>  lgMedianMap;
  std::map<int,TH2Poly*>  hgMeanMap;
  std::map<int,TH2Poly*>  lgMeanMap;
  std::map<int,TH2Poly*>  hgRMSMap;
  std::map<int,TH2Poly*>  lgRMSMap;
  std::map<int,TH2Poly*>  chanMap;
  std::ostringstream os( std::ostringstream::ate );
  TH2Poly *h;
  for(size_t ib = 0; ib<m_numberOfBoards; ib++) {
    os.str("");
    os << "HexaBoard" << ib ;
    TFileDirectory dir = fs->mkdir( os.str().c_str() );
    h=dir.make<TH2Poly>();
    os.str("");
    os<<"ChannelMapping";
    h->SetName(os.str().c_str());
    h->SetTitle(os.str().c_str());
    h->SetOption("colztext");
    InitTH2Poly(*h, ib, 0, 0);
    chanMap.insert( std::pair<int,TH2Poly*>(ib,h) );
    TFileDirectory hgpdir = dir.mkdir( "HighGainPedestal" );
    TFileDirectory lgpdir = dir.mkdir( "LowGainPedestal" );
    TFileDirectory hgpdir_mean = dir.mkdir( "HighGainPedestal_Mean" );
    TFileDirectory lgpdir_mean = dir.mkdir( "LowGainPedestal_Mean" );
    TFileDirectory hgndir = dir.mkdir( "HighGainNoise" );
    TFileDirectory lgndir = dir.mkdir( "LowGainNoise" );
    for( size_t it=0; it<NUMBER_OF_SCA; it++ ){

      h=hgpdir.make<TH2Poly>();
      os.str("");
      os<<"SCA_"<<it;
      h->SetName(os.str().c_str());
      h->SetTitle(os.str().c_str());
      h->SetOption("colztext");
      InitTH2Poly(*h, ib, 0, 0);
      hgMedianMap.insert( std::pair<int,TH2Poly*>(100*ib+it,h) );

      h=lgpdir.make<TH2Poly>();
      os.str("");
      os<<"SCA"<<it;
      h->SetName(os.str().c_str());
      h->SetTitle(os.str().c_str());
      h->SetOption("colztext");
      InitTH2Poly(*h, ib, 0, 0);
      lgMedianMap.insert( std::pair<int,TH2Poly*>(100*ib+it,h) );

      h=hgpdir_mean.make<TH2Poly>();
      os.str("");
      os<<"SCA_"<<it;
      h->SetName(os.str().c_str());
      h->SetTitle(os.str().c_str());
      h->SetOption("colztext");
      InitTH2Poly(*h, ib, 0, 0);
      hgMeanMap.insert( std::pair<int,TH2Poly*>(100*ib+it,h) );

      h=lgpdir_mean.make<TH2Poly>();
      os.str("");
      os<<"SCA"<<it;
      h->SetName(os.str().c_str());
      h->SetTitle(os.str().c_str());
      h->SetOption("colztext");
      InitTH2Poly(*h, ib, 0, 0);
      lgMeanMap.insert( std::pair<int,TH2Poly*>(100*ib+it,h) );

      h=hgndir.make<TH2Poly>();
      os.str("");
      os<<"SCA"<<it;
      h->SetName(os.str().c_str());
      h->SetTitle(os.str().c_str());
      h->SetOption("colztext");
      InitTH2Poly(*h, ib, 0, 0);
      hgRMSMap.insert( std::pair<int,TH2Poly*>(100*ib+it,h) );

      h=lgndir.make<TH2Poly>();
      os.str("");
      os<<"SCA"<<it;
      h->SetName(os.str().c_str());
      h->SetTitle(os.str().c_str());
      h->SetOption("colztext");
      InitTH2Poly(*h, ib, 0, 0);
      lgRMSMap.insert( std::pair<int,TH2Poly*>(100*ib+it,h) );
    }
  }

  for( std::map<int,hgcal_capacitor>::iterator it=m_channelMap.begin(); it!=m_channelMap.end(); ++it ){ //per channel: gets median + iqr of the ~800 ADC counts | output: 1 int per phys. SCA for median and iqr HG/LG -> 128 int
    it->second.meanHG = std::accumulate(std::begin(it->second.highGain), std::end(it->second.highGain), 0.0) / it->second.highGain.size(); it->second.meanLG = std::accumulate(std::begin(it->second.lowGain), std::end(it->second.lowGain), 0.0) / it->second.lowGain.size(); it->second.rmsHG = sqrt( ( std::inner_product(std::begin(it->second.highGain), std::end(it->second.highGain), std::begin(it->second.highGain), 0 ) ) / static_cast<double>(it->second.highGain.size() ) ); it->second.rmsLG = sqrt( ( std::inner_product(std::begin(it->second.lowGain), std::end(it->second.lowGain), std::begin(it->second.lowGain), 0 ) ) / static_cast<double>(it->second.lowGain.size() ) );
    std::sort( it->second.highGain.begin(),it->second.highGain.end() ); //List of all 800 HG T.S.0 ADC's
    std::sort( it->second.lowGain.begin(),it->second.lowGain.end() );
    unsigned int size = it->second.highGain.size();
    int medianIndex = int( 0.5*(size-1) );
    it->second.medianHG = it->second.highGain.at(medianIndex) ;
    it->second.medianLG = it->second.lowGain.at(medianIndex) ;
    int sigma_1_Index = int( 0.16*(size-1) );
    int sigma_3_Index = int( 0.84*(size-1) );
    // if((0.5*(it->second.highGain.at(sigma_3_Index)-it->second.highGain.at(sigma_1_Index))) < 10){
    it->second.iqrHG=0.5*(it->second.highGain.at(sigma_3_Index)-it->second.highGain.at(sigma_1_Index));
    // }
    it->second.iqrLG=0.5*(it->second.lowGain.at(sigma_3_Index)-it->second.lowGain.at(sigma_1_Index));
  }


  std::fstream pedestalHG;
  std::fstream pedestalLG;
  if( m_writePedestalFile ){
    pedestalHG.open(m_pedestalHigh_filename,std::ios::out);
    pedestalLG.open(m_pedestalLow_filename,std::ios::out);
  }

  int board, skiroc, channel, sca,module, layer, position, size, type, celltype;
  double median_high, IQR_high, mean_high, RMS_high, median_low, IQR_low, mean_low, RMS_low, iux, iuy;
  std::vector<float> adc_high, adc_low;
  TTree* noiseTree =fs->make<TTree>("noiseTree", "noiseTree");
  noiseTree->Branch("board", &board);
  noiseTree->Branch("skiroc", &skiroc);
  noiseTree->Branch("channel", &channel);
  noiseTree->Branch("sca", &sca);
  noiseTree->Branch("module",&module);
  noiseTree->Branch("median_high", &median_high);
  noiseTree->Branch("IQR_high", &IQR_high);
  noiseTree->Branch("mean_high", &mean_high);
  noiseTree->Branch("RMS_high", &RMS_high);
  if ( m_fillFullTree ){
    noiseTree->Branch("ADC_HighGain", &adc_high);
  }
  noiseTree->Branch("median_low", &median_low);
  noiseTree->Branch("IQR_low", &IQR_low);
  noiseTree->Branch("mean_low", &mean_low);
  noiseTree->Branch("RMS_low", &RMS_low);
  if ( m_fillFullTree ){
    noiseTree->Branch("ADC_LowGain", &adc_low);
  }
  noiseTree->Branch("layer", &layer);
  noiseTree->Branch("position", &position);
  noiseTree->Branch("type", &type);
  noiseTree->Branch("celltype", &celltype);
  noiseTree->Branch("size", &size);
  noiseTree->Branch("iuy", &iuy);
  noiseTree->Branch("iux", &iux);


  std::fstream noisyChannels;
  if( m_writeNoisyChannelFile )
    noisyChannels.open(m_noisyChannels_filename,std::ios::out);
  std::map<int,double> meanNoise;
  std::map<int,double> rmsNoise;
  std::map<int,int> countNoise;

  for( std::set< std::pair<int,HGCalTBDetId> >::iterator it=setOfConnectedDetId.begin(); it!=setOfConnectedDetId.end(); ++it ){ //loop over every connected channel (128)
    int iboard=(*it).first/1000;
    int iski=((*it).first%1000)/100;
    int ichan=(*it).first%100;
    if( m_writePedestalFile ){
      pedestalHG << iboard << " " << iski << " " << ichan ;
      pedestalLG << iboard << " " << iski << " " << ichan ;
    }
    HGCalTBDetId detid=(*it).second;
    CellCentreXY = TheCell.GetCellCentreCoordinatesForPlots( detid.layer(), 0, 0, detid.iu(), detid.iv(), m_sensorsize );


    iux = CellCentreXY.first;
    iuy = CellCentreXY.second;

    HGCalTBDetId detid2(-1);
    int skiId=HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA*(iboard)+(HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA-iski)%HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA+1;
    HGCalTBElectronicsId eid(skiId,ichan);
    int moduleId=-1;
    if (essource_.emap_.existsEId(eid.rawId())) {
      detid2 = essource_.emap_.eid2detId(eid);
      HGCalTBLayer layer = essource_.layout_.at(detid2.layer() - 1);
      moduleId = layer.at( detid2.sensorIU(), detid2.sensorIV() ).moduleID();

    }
    else{
      HGCalTBDetId did(-1);
      detid2=did;
    }
    board = iboard;
    skiroc = iski;
    channel = ichan;
    module=moduleId;
    layer=detid2.layer() ;
    int IU=detid2.sensorIU();
    int IV=detid2.sensorIV();
    position=-1;
    if(IU==1){
      if(IV==0) position=5;
      else if (IV==-1) position=6;
    }
    else if(IU==0){
      if(IV==1) position=1;
      else if (IV==0) position=4;
      else if (IV==-1) position=7;
    }
    else if(IU==-1){
      if(IV==1) position=2;
      else if (IV==0) position=3;
    }
    type=3;
    if(module==38 || module==39 || module==42 || module==45 || module==53 || module==46 || module==48 || module==75 || module==59 || module==71 || module==64 || module==55 || module==63 || module==66 || module==62 || module==43) type=2;
    else if(module==91 || module==130 || module==133 || module==131 || module==134 || module==132 || module==146) type=4;
    else if(module==141 || module==147 || module==137 || module==139 || module==136 || module==140 || module==138) type=5;
    size=300;
    if(module>143) size=200;
    celltype=detid2.cellType();


    for( size_t it=0; it<NUMBER_OF_SCA; it++ ){
      sca=it;
      int key=iboard*100000+iski*10000+ichan*100+it;
      std::map<int,hgcal_capacitor>::iterator iter=m_channelMap.find(key);
      float hgMedian=iter->second.medianHG;
      float lgMedian=iter->second.medianLG;
      float hgIQR=iter->second.iqrHG;
      float lgIQR=iter->second.iqrLG;
      float hgMean=iter->second.meanHG;
      float lgMean=iter->second.meanLG;
      float hgRMS=iter->second.rmsHG;
      float lgRMS=iter->second.rmsLG;

      hgMedianMap[ 100*iboard+it ]->Fill(iux , iuy, hgMedian );
      lgMedianMap[ 100*iboard+it ]->Fill(iux , iuy, lgMedian );
      hgMeanMap[ 100*iboard+it ]->Fill(iux , iuy, hgMean );
      lgMeanMap[ 100*iboard+it ]->Fill(iux , iuy, lgMean );
      hgRMSMap[ 100*iboard+it ]->Fill(iux , iuy, hgRMS );
      lgRMSMap[ 100*iboard+it ]->Fill(iux , iuy, lgRMS );

      median_high = hgMedian;
      IQR_high = hgIQR;
      mean_high = hgMean;
      RMS_high = hgRMS;
      median_low = lgMedian;
      IQR_low = lgIQR;
      mean_low = lgMean;
      RMS_low = lgRMS;

      if ( m_fillFullTree ){
        adc_high = iter->second.highGain;
        adc_low =  iter->second.lowGain;
      }
      noiseTree->Fill();

      if( m_writePedestalFile ){
      	pedestalHG << " " << hgMedian << " " << hgIQR;
	      pedestalLG << " " << lgMedian << " " << lgIQR;
      }
    }
    chanMap[ iboard ]->Fill(iux , iuy, iski*1000+ichan );
    if( m_writePedestalFile ){
      pedestalHG << std::endl;
      pedestalLG << std::endl;
    }
    if( m_writeNoisyChannelFile ){
      if( detid.cellType()!=0 && detid.cellType()!=5  && detid.cellType()!=4 )continue;
      int key=iboard*100000+iski*10000+ichan*100;
      std::map<int,hgcal_capacitor>::iterator iter=m_channelMap.find(key);
      float hgIQR=iter->second.iqrHG;
      if( meanNoise.find(iboard)==meanNoise.end() ){
	meanNoise[iboard]=hgIQR;
	rmsNoise[iboard]=hgIQR*hgIQR;
	countNoise[iboard]=1;
      }
      else{
	meanNoise[iboard]+=hgIQR;
	rmsNoise[iboard]+=hgIQR*hgIQR;
	countNoise[iboard]+=1;
      }
    }
  }
  if( m_writePedestalFile ){ // written in Pedestalplotter ONLY
    pedestalHG.close();
    pedestalLG.close();
  }
  if( m_writeNoisyChannelFile ){
    for( std::map<int,double>::iterator it=meanNoise.begin(); it!=meanNoise.end(); ++it ){
      meanNoise[ it->first ] = meanNoise[ it->first ]/countNoise[ it->first ];
      rmsNoise[ it->first ] = std::sqrt( rmsNoise[ it->first ]/countNoise[ it->first ] - meanNoise[ it->first ]*meanNoise[ it->first ] );
      std::cout << it->first << " " << meanNoise[ it->first ] << " " << rmsNoise[ it->first ] << std::endl;
    }
    for( std::set< std::pair<int,HGCalTBDetId> >::iterator it=setOfConnectedDetId.begin(); it!=setOfConnectedDetId.end(); ++it ){
      HGCalTBDetId detid=(*it).second;
      if( detid.cellType()!=0 && detid.cellType()!=4 )continue;
      int iboard=(*it).first/1000;
      int iski=((*it).first%1000)/100;
      int ichan=(*it).first%100;
      int key=iboard*100000+iski*10000+ichan*100;//we use SCA 0
      std::map<int,hgcal_capacitor>::iterator iter=m_channelMap.find(key);
      if( iter->second.iqrHG-meanNoise[iboard]>3*rmsNoise[iboard] )
	noisyChannels << iboard << " " << iski << " " << ichan << std::endl;
    }
    noisyChannels.close();
  }
}

void NoisePlotter::fillDescriptions(edm::ConfigurationDescriptions& descriptions)
{
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);
}

DEFINE_FWK_MODULE(NoisePlotter);
