import FWCore.ParameterSet.Config as cms
import FWCore.ParameterSet.VarParsing as VarParsing

import os,sys
import yaml

options = VarParsing.VarParsing('standard') # avoid the options: maxEvents, files, secondaryFiles, output, secondaryOutput because they are already defined in 'standard'
#Change the data folder appropriately to where you wish to access the files from:

options.register('fileName',
                 '/afs/cern.ch/user/p/psiebere/CMSSW_8_0_1/src/HGCal/comparison/raw/HPc02_Dummy2_HVscan_wInterposer_2019-3-27_17-9-24.raw',
                 VarParsing.VarParsing.multiplicity.singleton,
                 VarParsing.VarParsing.varType.string,
                 'Input file (path needed) to process')

options.register('edmOutputFolder',
                 '/afs/cern.ch/user/p/psiebere/CMSSW_8_0_1/src/HGCal/comparison/for_talk/',
                 VarParsing.VarParsing.multiplicity.singleton,
                 VarParsing.VarParsing.varType.string,
                 'Output folder where analysis output are stored')

options.register('analysisOutputFolder',
                 '/afs/cern.ch/user/p/psiebere/CMSSW_8_0_1/src/HGCal/comparison/for_talk/',
                 VarParsing.VarParsing.multiplicity.singleton,
                 VarParsing.VarParsing.varType.string,
                 'Output folder where analysis output are stored')

options.register('compressedData',
                 True,
                 VarParsing.VarParsing.multiplicity.singleton,
                 VarParsing.VarParsing.varType.bool,
                 'Option to set if the data have beem compressed')

options.register('electronicMap',
                 "HGCal/CondObjects/data/map_CERN_Hexaboard_OneModule_V3.txt",
                 VarParsing.VarParsing.multiplicity.singleton,
                 VarParsing.VarParsing.varType.string,
                 'path to the electronic map')

options.maxEvents = -1

options.parseArguments()

aname=os.path.basename(options.fileName)
aname,extent=os.path.splitext(aname)
yamlfile=options.analysisOutputFolder+"/../yaml/"+aname+".yaml"
aname=aname+"_edm.root"
options.output=options.edmOutputFolder+aname

print options

pedestalHighGain=options.analysisOutputFolder+"/pedestalHG.txt"
pedestalLowGain=options.analysisOutputFolder+"/pedestalLG.txt"
noisyChannels=options.analysisOutputFolder+"/noisyChannels.txt"

################################
process = cms.Process("unpack")
process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(options.maxEvents)
)
####################################
# Reduces the frequency of event count couts
# process.load("FWCore.MessageLogger.MessageLogger_cfi")
# process.MessageLogger.cerr.FwkReport.reportEvery = options.reportEvery

####################################

numberOfBytesForTheHeader=48
print("yamlfile = ",yamlfile)
with open(yamlfile) as fin:
    yf=yaml.safe_load(fin)
    daq_options=yf['daq_options']
    for param in daq_options.iteritems():
        if 'headerSize' in param:
            numberOfBytesForTheHeader=daq_options['headerSize']

numberOfBytesForTheEventTrailers=2
numberOfBytesPerReadOut=30784
if options.compressedData==1:
    numberOfBytesPerReadOut=15392
process.source = cms.Source("HGCalTBRawDataSource",
                            ElectronicMap=cms.untracked.string(options.electronicMap),
                            fileNames=cms.untracked.vstring("file:%s"%(options.fileName)),
                            OutputCollectionName=cms.untracked.string("skiroc2cmsdata"),
                            NumberOfBytesPerReadOut=cms.untracked.uint32(numberOfBytesPerReadOut),
                            NumberOfBytesForTheHeader=cms.untracked.uint32(numberOfBytesForTheHeader),
                            NumberOfBytesForTheEventTrailers=cms.untracked.uint32(numberOfBytesForTheEventTrailers),
                            NSkipEvents=cms.untracked.uint32(0),
                            CompressedData=cms.untracked.bool(options.compressedData)
)

outputFileName=os.path.basename(options.fileName)
outputFileName,extent=os.path.splitext(outputFileName)
outputFileName=options.analysisOutputFolder+outputFileName+"_pedestal.root"
process.TFileService = cms.Service("TFileService", fileName=cms.string(outputFileName))

process.output = cms.OutputModule("PoolOutputModule",fileName = cms.untracked.string(options.output))

process.content = cms.EDAnalyzer("EventContentAnalyzer") #add process.content in cms.Path if you want to check which collections are in the event

process.noiseplotter = cms.EDAnalyzer("NoisePlotter",
                                   InputCollection=cms.InputTag("source","skiroc2cmsdata"),
                                   ElectronicMap=cms.untracked.string(options.electronicMap),
                                   SensorSize=cms.untracked.int32(128),
                                   WritePedestalFile=cms.untracked.bool(False),
                                   HighGainPedestalFileName=cms.untracked.string(pedestalHighGain),
                                   LowGainPedestalFileName=cms.untracked.string(pedestalLowGain),
                                   WriteNoisyChannelsFile=cms.untracked.bool(False),
                                   NoisyChannelsFileName=cms.untracked.string(noisyChannels),
                                   WriteTreeOutput=cms.untracked.bool(False),
                                   DetectorLayout=cms.untracked.string("HGCal/CondObjects/data/layerGeom_oct2017_h2_17layers.txt"),
                                   NTSForPedestalComputation=cms.untracked.int32(7),
                                   fillFullTree=cms.untracked.bool(True)
)

process.p = cms.Path( process.noiseplotter) #*process.treeproducer*process.rawhitproducer )

process.end = cms.EndPath(process.output)
